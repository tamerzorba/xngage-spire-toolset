const { ask, clear, prompt, exit } = require('../helpers');
const fs = require('fs');
const path = require('path');
const Page = require('./page');
const Soon = require('./soon');

const chooseBlueprint = async () => {
  const fPath = path.join(process.cwd(), 'modules/blueprints');
  if (!fs.existsSync(fPath)) {
    return exit('you need to run this command at the root of spire project');
  }

  const list = fs
    .readdirSync(fPath)
    .filter((f) => fs.lstatSync(path.join(fPath, f)).isDirectory());

  const blueprint = await ask({
    question: 'Choose your blueprint ?',
    answers: list,
  });

  generate(blueprint.value);
};

const generate = async (blueprint) => {
  const q = await ask({
    question: `What to Generate for ${blueprint} ?`,
    answers: [
      'Page', // inside it as new and content
      'Page Template', // inside it as new and content
      'Widget',
      'Component',
      'Extention',
      'Handler',
      'Reducer',
      'Store',
      'Service',
      'StyleGuide',
    ],
  });

  switch (q.value) {
    case 'Page':
      Page(blueprint, q.value);
      break;

    default:
      Soon(blueprint, q.value);
  }
};

module.exports = () => {
  try {
    chooseBlueprint();
  } catch (err) {
    clear();
    if (err) console.log({ err });
    console.log(
      'you have canceled the action, thank you for using xngage toolset'
    );
  }
};
