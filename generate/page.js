const { template, prompt, randomGuid } = require('../helpers');
const {
  existTryAgain,
  confirmName,
  getBlueprintPath,
  write,
} = require('./shared');
const camelCase = require('camelcase');
const clc = require('cli-color');
const path = require('path');
const fse = require('fs-extra');

const generatePage = async (blueprint, type) => {
  const name = await prompt(`what is your ${type} name ?`);
  const PageName = camelCase(name.value + '-page', { pascalCase: true });

  const blueprintPath = getBlueprintPath(blueprint);

  const files = {
    script: path.join(blueprintPath, 'src/Pages', PageName + '.tsx'),
    template: path.join(
      blueprintPath,
      'wwwroot/AppData/PageTemplates/',
      PageName,
      '/Standard.json'
    ),
  };

  if ((await existTryAgain(files, PageName)) || (await confirmName(PageName))) {
    return generatePage(blueprint, type);
  }

  // writing script
  const scriptData = template('page.script')({ pageName: PageName });
  write(files.script, scriptData, blueprintPath);

  // writing template
  const templateData = template('page.template')({
    pageName: PageName,
    id: randomGuid(),
    nodeId: randomGuid(),
    parentId: randomGuid(),
    websiteId: randomGuid(),
    translatableId: randomGuid(),
    widgetId1: randomGuid(),
    widgetId2: randomGuid(),
    conetntId: randomGuid(),
  });
  write(files.template, templateData, blueprintPath);

  console.log(clc.bold('Thank you for using Xngage Toolset Generators\n'));
};

module.exports = generatePage;
