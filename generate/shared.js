const { ask, exit } = require('../helpers');
const path = require('path');
const clc = require('cli-color');
const fse = require('fs-extra');

const Shared = {
  /**
   * blueprint path
   */
  getBlueprintPath: (blueprint) =>
    path.join(process.cwd(), 'modules/blueprints', blueprint),

  /**
   * check existence
   */
  existTryAgain: async (files, PageName) => {
    // check for files existence
    const exists = Object.values(files).some((file) => fse.existsSync(file));
    if (exists) {
      const overwrite = await ask({
        question: clc.red(
          `Another Page called ${PageName} already exists, do you want to overwrite ?`
        ),
        answers: ['No', 'Yes'],
      });
      if (overwrite.value == 'No') {
        const again = await ask({
          question: `will not overwrite ${PageName}.\n\n do you want to try another name ?`,
          answers: ['Yes', 'No, cancel'],
        });
        if (again.value == 'Yes') {
          return true;
        } else {
          exit();
        }
      }
    }
  },

  /**
   * confirm name
   */
  confirmName: async (PageName) => {
    // confirm name
    const confirmName = await ask({
      question: `will generate ${clc.green.bold(PageName)}, are you sure ?`,
      answers: ['Yes', 'No, let me change it'],
    });
    if (confirmName.id == 1) {
      return true;
    }
  },

  /**
   * write files
   */
  write: (filePath, templateData, blueprintPath) => {
    console.log(
      clc.bold(`Generating ${filePath.substr(blueprintPath.length)}`)
    );
    fse.outputFileSync(filePath, templateData);
    console.log(clc.green.bold('Success\n'));
  },
};

module.exports = Shared;
