const cliSelect = require('cli-select');
const clc = require('cli-color');
const prompts = require('prompts');
const fs = require('fs');
const path = require('path');

const Helpers = {
  /**
   * interpolate
   * replace {{variables}} interpolations in template
   *
   * @param {string} templateText
   * @param {object} data
   *
   * @usage interpolate(templateText)({key:value}):string
   */
  interpolate: (str) => (o) =>
    str.replace(/\{{([^{}]*)}}/g, (a, b) => {
      var r = o[b];
      return typeof r === 'string' || typeof r === 'number' ? r : a;
    }),

  /**
   * template
   * load templates by name from path
   *
   * @param {number} name
   * @param {any} data
   *
   * @usage template("template.name", {key:value}):string
   */
  template: (name, data = {}) =>
    Helpers.interpolate(
      fs
        .readFileSync(
          path.join(__dirname, `/templates/${name.replace('.', '/')}.tpl`)
        )
        .toString(),
      data
    ),

  /**
   * default question parameters
   */
  defaultOptions: {
    selected: clc.green('◉'),
    unselected: '○',
    indentation: 2,
  },

  /**
   * ask
   * cli multiple question selector
   *
   * @param {object} question
   *
   * @usage ask({ question: "question", answers:['ans1', 'ans2'] }):promise
   */
  ask: ({ question, answers, options = {} }) => {
    console.log(clc.red('›') + ' ' + question);
    const promise = cliSelect({
      ...Helpers.defaultOptions,
      ...options,
      values: answers,
      valueRenderer: (value, selected) => {
        if (selected) {
          return clc.green.italic(value);
        }
        return value;
      },
    });
    promise
      .then((e) => {
        console.log(clc.green.bold('› ' + e.value) + '\n');
      })
      .catch((err) => {});
    return promise;
  },

  /**
   * clear
   * clean cli page
   *
   * @usage clear():void
   */
  clear: () => {
    process.stdout.write(clc.reset);
  },

  /**
   * prompt
   * cli input question
   *
   * @param {string} question
   * @param {number} minLength
   *
   * @usage prompt("question"):promise
   */
  prompt: (question, minLength = 3) => {
    return prompts({
      type: 'text',
      name: 'value',
      message: question,
      validate: (value) =>
        value.length < minLength
          ? `Should be more than ${minLength} Characters`
          : true,
    });
  },

  /**
   * randomId
   * generate random id, default is hex
   *
   * @param {number} length
   * @param {any} characters
   *
   * @usage randomId(4):string
   */
  randomId: function (length = 10, characters = 'abcdef0123456789') {
    let result = '';
    for (var i = 0; i < length; i++) {
      result += characters.charAt(
        Math.floor(Math.random() * characters.length)
      );
    }
    return result;
  },

  /**
   * randomGuid
   * generate random guid based on template
   *
   * @param {array} pattern
   * @param {string} seperator
   *
   * @usage randomGuid([8, 4, 4, 4, 12], '-'):string
   */
  randomGuid: (pattern = [8, 4, 4, 4, 12], seperator = '-') => {
    return pattern.map((n) => Helpers.randomId(n)).join(seperator);
  },

  /**
   * exit with thanx message
   */
  exit: (msg = 'Bye Bye, thank you for using Xngage Toolset') => {
    msg && console.log(msg);
    process.exit();
  },
};

Helpers.clear();
module.exports = Helpers;
