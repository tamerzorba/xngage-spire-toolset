#!/usr/bin/env node

const { ask, template, clear, exit } = require('./helpers');
const generate = require('./generate');

(async () => {
  try {
    console.log(template('logo')({ year: '2020' }));

    const start = await ask({
      question: 'Welcome to Xngage Toolset, choose your Tool ?',
      answers: ['Generate', 'About'],
    });

    if (start.id == 0) {
      generate();
    }
    if (start.id == 1) {
      clear();
      console.log(template('about')({ year: '2020' }));
    }
  } catch (err) {
    if (err) console.log({ err });
    exit();
  }
})();
