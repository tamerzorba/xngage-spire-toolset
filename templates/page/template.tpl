{
  "id": "{{id}}",
  "nodeId": "{{nodeId}}",
  "name": "Page",
  "type": "{{pageName}}",
  "parentId": "{{parentId}}",
  "sortOrder": -1,
  "websiteId": "{{websiteId}}",
  "generalFields": {
    "horizontalRule": "",
    "hideHeader": false,
    "hideFooter": false,
    "hideFromSearchEngines": false,
    "hideFromSiteSearch": false,
    "excludeFromNavigation": false,
    "excludeFromSignInRequired": false,
    "tags": []
  },
  "translatableFields": {
    "title": {
      "{{translatableId}}": "Page"
    },
    "openGraphTitle": {
      "{{translatableId}}": ""
    },
    "urlSegment": {
      "{{translatableId}}": "Page"
    },
    "openGraphUrl": {
      "{{translatableId}}": ""
    },
    "openGraphImage": {
      "{{translatableId}}": ""
    },
    "metaKeywords": {
      "{{translatableId}}": ""
    },
    "metaDescription": {
      "{{translatableId}}": ""
    }
  },
  "contextualFields": {},
  "widgets": [
    {
      "id": "{{widgetId1}}",
      "parentId": "{{id}}",
      "type": "Basic/PageTitle",
      "zone": "Content",
      "generalFields": {},
      "translatableFields": {},
      "contextualFields": {}
    },
    {
      "id": "{{widgetId2}}",
      "parentId": "{{id}}",
      "type": "Basic/RichContent",
      "zone": "Content",
      "generalFields": {},
      "translatableFields": {},
      "contextualFields": {
        "content": {
          "{{translatableId}}|Desktop|{{conetntId}}": "{{name}} Page Content!"
        }
      }
    }
  ]
}
